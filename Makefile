#
# Makefile for installing the Fedora/RHEL specific s390(x) stuff
#

PACKAGE=s390utils
VERSION=0.1

DESTDIR=
BINDIR=$(DESTDIR)/bin
SBINDIR=$(DESTDIR)/sbin
USRBINDIR=$(DESTDIR)/usr/bin
USRSBINDIR=$(DESTDIR)/usr/sbin
UDEVDIR=$(DESTDIR)/lib/udev
SYSCONFIGDIR=$(DESTDIR)/etc


UDEV_RULES=dasd.udev zfcp.udev ccw.udev
UDEV_HELPERS=ccw_init

SERVICES=cpi.initd
SYSCONFIGS=cpi.sysconfig

SBIN_SCRIPTS=device_cio_free dasdconf.sh zfcpconf.sh

UPSTART_CONFIGS=device_cio_free.conf

FILES=Makefile $(UDEV_RULES) $(UDEV_HELPERS) $(SERVICES) $(SYSCONFIGS) $(SCRIPTS) $(UPSTART_CONFIGS)


all:

install:
	mkdir -p $(BINDIR) \
		 $(SBINDIR) \
		 $(USRBINDIR) \
		 $(USRSBINDIR) \
		 $(UDEVDIR)/rules.d \
		 $(SYSCONFIGDIR) \
		 $(SYSCONFIGDIR)/rc.d/init.d \
		 $(SYSCONFIGDIR)/sysconfig \
		 $(SYSCONFIGDIR)/init
	install -p -m 644 $(UDEV_RULES) $(UDEVDIR)/rules.d
	install -p -m 755 $(UDEV_HELPERS) $(UDEVDIR)
	install -p -m 644 $(SERVICES) $(SYSCONFIGDIR)/rc.d/init.d
	install -p -m 644 $(SYSCONFIGS) $(SYSCONFIGDIR)/sysconfig
	install -p -m 755 $(SBIN_SCRIPTS) $(SBINDIR)
	install -p -m 644 $(UPSTART_CONFIGS) $(SYSCONFIGDIR)/init

dist:
	mkdir -p $(PACKAGE)-$(VERSION)
	cp -p $(FILES) $(PACKAGE)-$(VERSION)
	tar cjf $(PACKAGE)-$(VERSION).tar.bz2 $(PACKAGE)-$(VERSION)
	rm -rf $(PACKAGE)-$(VERSION)
